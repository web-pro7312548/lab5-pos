import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
  const currentUser = ref<User>({
    id: 1,
    email: 'mamam@gmail.com',
    password: 'Pass@1234',
    fullName: 'มาดี มานะ',
    gender: 'male',
    roles: ['user']
  })
  return { currentUser }
})
